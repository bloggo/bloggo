import { log } from "./log";

export async function wait(seconds: number) {
  for (let index = 0; index < seconds; index++) {
    await log(`Waiting ${index + 1} / ${seconds} s.`);
    await new Promise(resolve => setTimeout(resolve, 1000));
  }
}
