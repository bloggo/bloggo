import fetch, { Headers } from 'node-fetch';
import * as base64 from 'base-64';
import * as nodeURL from 'url';

void async function(blogTitle, postTitle, postLink, subscriberEmail) {
  const apiKey = process.env.MAILGUN_API_KEY;
  if (!apiKey) {
    console.log('No MailGun API key found at MAILGUN_API_KEY.');
    return;
  }

  const headers = new Headers();
  headers.append('Authorization', `Basic ${base64.encode('api:' + apiKey)}`);
  const url = new nodeURL.URL('https://api.mailgun.net/v3/bloggo.hubelbauer.net/messages');
  url.searchParams.append('from', 'Tomas Hubelbauer <mailgun@bloggo.hubelbauer.net>');
  url.searchParams.append('to', 'Tomas Hubelbauer <tomas@hubelbauer.net>');
  url.searchParams.append('subject', '${postTitle} on ${blogTitle} has been updated!');
  url.searchParams.append('text', '${postTitle} on ${blogTitle} has been updated! Go check it now ${postLink}');
  await fetch(url.toString(), { headers, method: 'POST' });
}()
