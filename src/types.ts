type Entry = {
  name: string;
  isDirectory: boolean;
};

type Change = {
  author: {
    timestamp: number;
  },
  committer: {
    timestamp: number;
  },
  webUrl: string;
  oid: string;
};

type Configuration = Partial<{
  title: string;
  entry: string;
  template: string;
  syncIntervalMs: number;
}>;
