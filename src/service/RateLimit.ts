export default class RateLimit {
  public readonly limit: number;
  public readonly remaining: number;
  public readonly reset: Date;

  constructor(limit: number, remaining: number, reset: Date) {
    this.limit = limit;
    this.remaining = remaining;
    this.reset = reset;
  }

  public toString() {
    if (this.remaining === 0) {
      return `${this.remaining} / ${this.limit}; reset: ${this.reset.toLocaleString()}`;
    }

    return `${this.remaining} / ${this.limit}`;
  }
}
