import GitHubService from './GitHubService';
import ServiceRepository from './ServiceRepository';

export default class GitHubUserService extends GitHubService {
  constructor(handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void) {
    super('GitHub (User)', handle, onRepositoryRemoved, 'users');
  }
}
