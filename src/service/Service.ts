import ServiceRepository from './ServiceRepository';
import RateLimit from './RateLimit';
import fetch, { Response } from 'node-fetch';

export default abstract class Service {
  public readonly name: string;
  public readonly handle: string;
  public readonly onRepositoryRemoved: (repository: ServiceRepository) => void;

  public rateLimit: RateLimit | null = null;

  protected list: ServiceRepository[] = [];

  constructor(name: string, handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void) {
    this.name = name;
    this.handle = handle;
    this.onRepositoryRemoved = onRepositoryRemoved;
  }

  protected updateRepositoriesAndReportRemoved(repositories: ServiceRepository[]) {
    for (const repository of this.list) {
      if (!repositories.find(r => r.name === repository.name)) {
        this.onRepositoryRemoved(repository);
      }
    }

    this.list = repositories;
  }

  public abstract walkRepositories(): AsyncIterableIterator<ServiceRepository>;

  public updateRateLimit(rateLimit: RateLimit) {
    this.rateLimit = rateLimit;
    if (this.rateLimit.remaining === 0) {
      throw new Error('Rate limit reached!');
    }
  }

  public toString() {
    return `${this.name} ${this.handle}`;
  }
}
