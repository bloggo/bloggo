import fetch from 'node-fetch';
import Service from './Service';
import GitHubRepository from './GitHubRepository';
import { log } from '../log';
import { Response } from 'node-fetch';
import RateLimit from './RateLimit';
import ServiceRepository from './ServiceRepository';
import { getRateLimitAndPaginationStatus } from './github';

type Repository = { id: string; name: string; description: string; clone_url: string; ssh_url: string; html_url: string; created_at: number; size: number; };

export default class GitHubService extends Service {
  private readonly type: 'users' | 'orgs';

  constructor(name: string, handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void, type: 'users' | 'orgs') {
    super(name, handle, onRepositoryRemoved);
    this.type = type;
  }

  public async *walkRepositories() {
    await log(`Fetching ${this.toString()}`);
    let fullList: GitHubRepository[] = [];
    let page = '';
    do {
      await log(`Paging ${this.toString()} (${page || 'initial'})`);
      const response = await fetch(`https://api.github.com/${this.type}/${this.handle}/repos?type=owner&per_page=100&page=` + page);
      const { rateLimit, nextPage } = getRateLimitAndPaginationStatus(response);
      page = nextPage;
      this.updateRateLimit(rateLimit);

      const data: Repository[] = await response.json();
      for (const item of data) {
        const repository = new GitHubRepository(item.id, item.name, item.description, item.clone_url, item.ssh_url, item.html_url, new Date(item.created_at), item.size === 0, this);
        fullList.push(repository);
        yield repository;
      }
    } while (page);
    this.updateRepositoriesAndReportRemoved(fullList);
  }
}
