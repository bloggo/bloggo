import * as path from 'path';
import * as fs from 'fs-extra';
import * as git from 'isomorphic-git';
import * as showdown from 'showdown';
import { log } from '../log';
import Service from './Service';

export default abstract class ServiceRepository {
  static converter = new showdown.Converter({ tasklists: true, tables: true });

  public readonly id: string;
  public readonly rawName: string;
  public readonly name: string;
  public readonly description: string;
  public readonly gitHttpUrl: string;
  public readonly gitSshUrl: string;
  public readonly webUrl: string;
  public readonly date: Date;
  public readonly isEmpty: boolean;
  public readonly service: Service;

  public html: string | null = null;
  public title: string | null = null;
  public changeset: (git.CommitDescription & { webUrl?: string })[] = [];
  public configuration: Configuration = {};
  public template: string | null = null;

  public readonly path: string;

  constructor(
    id: string,
    name: string,
    description: string,
    gitHttpUrl: string,
    gitSshUrl: string,
    webUrl: string,
    date: Date,
    isEmpty: boolean,
    service: Service
  ) {
    this.id = id;
    this.rawName = name;
    this.name = name.startsWith('bloggo-') ? name.substr('bloggo-'.length) : name;
    this.description = description;
    this.gitHttpUrl = gitHttpUrl;
    this.gitSshUrl = gitSshUrl;
    this.webUrl = webUrl;
    this.date = date;
    this.isEmpty = isEmpty;
    this.service = service;
    this.path = `data/${service.name}/${service.handle}/${name}`;
  }

  abstract getEntries(): Promise<Entry[]>;

  public async ensureUpToDateDirectory() {
    await fs.remove(this.path);
    if (await fs.pathExists(this.path)) {
      await log(`Pulling in ${this.path}…`);
      if (await Promise.race([
        git.pull({ fs, dir: this.path, ref: 'master' }),
        new Promise(resolve => setTimeout(resolve, 60000, 'timeout'))
      ]) === 'timeout') {
        await log(`Timeouting pull of ${this.path}`);
      }
    } else {
      await log(`Cloning from ${this.gitHttpUrl} to ${this.path}…`);
      if (await Promise.race([
        git.clone({ fs, dir: this.path, ref: 'master', url: this.gitHttpUrl }),
        new Promise(resolve => setTimeout(resolve, 60000, 'timeout'))
      ]) === 'timeout') {
        await log(`Timeouting clone of ${this.path}`);
      }
    }
  }

  public async removeDirectory() {
    await fs.remove(this.path);
  }

  public async refreshHtml() {
    const entryFilePath = path.join(this.path, this.configuration.entry || 'README.md');
    try {
      if (entryFilePath.endsWith('.md')) {
        const markdown = String(await fs.readFile(entryFilePath));
        this.html = ServiceRepository.converter.makeHtml(markdown);
      } else {
        this.html = String(await fs.readFile(entryFilePath));
      }
    } catch (error) {
      await log(`Failing ${this.toString()}: ${error}`);
      this.html = null;
    }
  }

  public refreshTitle() {
    const match = this.html && this.html.match(/<h1 id="\w+">(.*)<\/h1>/);
    if (match && match[1]) {
      this.title = match[1];
    } else {
      this.title = this.name;
    }
  }

  public async refreshChangeset() {
    this.changeset = await git.log({ fs, dir: this.path, ref: 'master' });
  }

  public async refreshTemplate() {
    const templateFilePath = path.join(this.path, this.configuration.template || '.bloggo/template.html');
    try {
      this.template = String(await fs.readFile(templateFilePath));
    } catch (error) {
      this.template = null;
    }
  }

  public async refreshConfiguration() {
    const configurationFilePath = path.join(this.path, '.bloggo/configuration.json');
    if (await fs.pathExists(configurationFilePath)) {
      this.configuration = (await fs.readJson(configurationFilePath, { throws: false })) || {};
    } else {
      this.configuration = {};
    }
  }

  public toString() {
    return `${this.service.toString()} > ${this.name}`;
  }
}
