import GitHubService from './GitHubService';
import ServiceRepository from './ServiceRepository';

export default class GitHubOrgService extends GitHubService {
  constructor(handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void) {
    super('GitHub (Org)', handle, onRepositoryRemoved, 'orgs');
  }
}
