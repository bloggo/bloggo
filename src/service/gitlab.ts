import { Response } from "node-fetch";
import RateLimit from "./RateLimit";

export function getRateLimitAndPaginationStatus(response: Response) {
  return {
    rateLimit: new RateLimit(
      Number(response.headers.get('ratelimit-limit')),
      Number(response.headers.get('ratelimit-remaining')),
      new Date(Number(response.headers.get('ratelimit-reset')) * 1000)
    ),
    nextPage: response.headers.get('x-next-page')
  };
}
