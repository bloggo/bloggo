import GitLabService from "./GitLabService";
import ServiceRepository from "./ServiceRepository";

export default class GitLabGroupService extends GitLabService {
  constructor(handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void) {
    super('GitLab (Group)', handle, onRepositoryRemoved, 'groups');
  }
}
