import fetch from 'node-fetch';
import ServiceRepository from "./ServiceRepository";
import GitLabService from "./GitLabService";
import { getRateLimitAndPaginationStatus } from './gitlab';

export default class GitLabRepository extends ServiceRepository {
  constructor(
    id: string,
    name: string,
    description: string,
    gitHttpUrl: string,
    gitSshUrl: string,
    webUrl: string,
    date: Date,
    isEmpty: boolean,
    service: GitLabService
  ) {
    super(id, name, description, gitHttpUrl, gitSshUrl, webUrl, date, isEmpty, service);
  }

  public async getEntries(): Promise<Entry[]> {
    const response = await fetch(`https://gitlab.com/api/v4/projects/${this.id}/repository/tree`);
    // Ignore pagination as we're looking for a dotdirectory which is bound to be at the start.
    const headers = getRateLimitAndPaginationStatus(response);
    const { rateLimit, nextPage } = getRateLimitAndPaginationStatus(response);
    this.service.updateRateLimit(rateLimit);
    const data: { name: string; type: string; }[] = await response.json();
    return data.map(item => ({ name: item.name, isDirectory: item.type === 'tree' }));
  }

  public async refreshChangeset() {
    await super.refreshChangeset();
    for (const change of this.changeset) {
      change.webUrl = this.webUrl + '/commit/' + change.oid;
    }
  }
}
