import fetch from 'node-fetch';
import Service from './Service';
import GitLabRepository from './GitLabRepository';
import { log } from '../log';
import { Response } from 'node-fetch';
import RateLimit from './RateLimit';
import ServiceRepository from './ServiceRepository';
import { getRateLimitAndPaginationStatus } from './gitlab';

type Repository = { id: string; name: string; description: string; http_url_to_repo: string; ssh_url_to_repo: string; web_url: string; created_at: number; default_branch: string; };

export default class GitLabService extends Service {
  private readonly type: 'users' | 'groups';

  constructor(name: string, handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void, type: 'users' | 'groups') {
    super(name, handle, onRepositoryRemoved);
    this.type = type;
  }

  public async *walkRepositories() {
    await log(`Fetching ${this.toString()}`);
    let fullList: GitLabRepository[] = [];
    let page = '';
    do {
      await log(`Paging ${this.toString()} (${page || 'initial'})`);
      const response = await fetch(`https://gitlab.com/api/v4/${this.type}/${this.handle}/projects?owned=true&per_page=100&page=` + page);
      const { rateLimit, nextPage } = getRateLimitAndPaginationStatus(response);
      page = nextPage;
      this.updateRateLimit(rateLimit);

      const data: Repository[] = await response.json();
      for (const item of data) {
        const repository = new GitLabRepository(item.id, item.name, item.description, item.http_url_to_repo, item.ssh_url_to_repo, item.web_url, new Date(item.created_at), item.default_branch === null, this);
        fullList.push(repository);
        yield repository;
      }
    } while (page);
    this.updateRepositoriesAndReportRemoved(fullList);
  }
}
