import GitLabService from "./GitLabService";
import ServiceRepository from "./ServiceRepository";

export default class GitLabUserService extends GitLabService {
  constructor(handle: string, onRepositoryRemoved: (repository: ServiceRepository) => void) {
    super('GitLab (User)', handle, onRepositoryRemoved, 'users');
  }
}
