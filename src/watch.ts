import * as timers from 'timers';
import Service from './service/Service';
import ServiceRepository from './service/ServiceRepository';
import { log, clear } from './log';
import { wait } from './wait';

// TODO: Change this to accept either `.bloggo/configuration.json` directory or `.bloggo` file or directory [ .gitkeep] (which could be empty placeholder)?
export default async function* watch(services: Service[], delay: number) {
  await log(`Watching:\n${services.map(service => service.toString()).join('\n')}\n`);
  do {
    await clear();
    let failures = 0;
    for (const service of services) {
      await log(`Querying ${service.toString()}`);
      try {
        for await (const repository of service.walkRepositories()) {
          await log(`Considering ${repository.toString()}`);
          if (repository.isEmpty) {
            continue;
          }

          if (repository.rawName.startsWith('bloggo-')) {
            await log(`Accepting ${repository.toString()}`);
            yield repository;
          } else {
            try {
              await log(`Searching ${repository.toString()}`);
              const entries = await repository.getEntries();
              const entry = entries.find(entry => entry.name === '.bloggo');
              if (entry && entry.isDirectory) {
                await log(`Accepting ${repository.toString()}`);
                yield repository;
              } else {
                await log(`Skipping ${repository.toString()}`);
              }
            } catch (error) {
              failures++;
              // Switch to the next service in case we hit the rate limit while fetching repository entries (no other repositories from the same service will work either)
              await log(`Failing ${repository.toString()} getEntries (rate limit: ${service.rateLimit && service.rateLimit.toString()}) ${error}`);
              break;
            }
          }
        }
      } catch (error) {
        failures++;
        // Switch to the next service in case we hit the rate limit while fetching repositories
        await log(`Failing ${service.toString()} walkRepositories (rate limit: ${service.rateLimit && service.rateLimit.toString()}) ${error}`);
      }
    }

    if (failures >= services.length) {
      const reset = services.filter(s => s.rateLimit !== null).sort((a, b) => a.rateLimit!.reset.getTime() - b.rateLimit!.reset.getTime())[0].rateLimit!.reset;
      const seconds = Math.ceil((reset.getTime() - Date.now()) / 1000);
      await wait(seconds);
    } else {
      await wait(delay);
    }

  } while (true);
}
