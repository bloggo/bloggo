import * as express from 'express';
import * as commander from 'commander';
import * as fs from 'fs-extra';
import { isValidName, executeJavaScriptFunction, getDefaultBlogTemplateString, getDefaultPostTemplateString } from './data';
import { log, clear } from './log';
import watch from './watch';
import GitLabUserService from './service/GitLabUserService';
import GitLabGroupService from './service/GitLabGroupService';
import GitHubUserService from './service/GitHubUserService';
import GitHubOrgService from './service/GitHubOrgService';
import ServiceRepository from './service/ServiceRepository';
import Service from './service/Service';

(<any>Symbol).asyncIterator = Symbol.asyncIterator || Symbol.for("Symbol.asyncIterator");

const { githubUser, githubOrg, gitlabUser, gitlabGroup, mock } = commander
  .version('1.0.0')
  .description('Bloggo')
  .option('--gitlab-user [handle]', 'GitLab user handle')
  .option('--gitlab-group [handle]', 'GitLab group handle')
  .option('--github-user [handle]', 'GitHub user handle')
  .option('--github-org [handle]', 'GitHub org handle')
  .option('--mock', 'Use mock data')
  .parse(process.argv);

if (!gitlabUser && !gitlabGroup && !githubUser && !githubOrg) {
  console.log('At least one of GitLab or GitHub handles must be set.');
  process.exit(1);
}

const versionHtml = '<!-- Bloggo v' + fs.readJsonSync('package.json').version + ' -->';

const services: Service[] = [];
if (gitlabUser) services.push(new GitLabUserService(gitlabUser, onRepositoryRemoved));
if (gitlabGroup) services.push(new GitLabGroupService(gitlabGroup, onRepositoryRemoved));
if (githubUser) services.push(new GitHubUserService(githubUser, onRepositoryRemoved));
if (githubOrg) services.push(new GitHubOrgService(githubOrg, onRepositoryRemoved));

async function onRepositoryRemoved(repository: ServiceRepository) {
  const index = repositories.indexOf(repository);
  repositories.splice(index, 1);
  await repository.removeDirectory();
  // TODO: Stop serving static assets by replacing router https://github.com/expressjs/express/issues/2596
  await log(`Deleted repository ${repository.name}.`);
}

let configuration: Configuration = {};
const repositories: ServiceRepository[] = [];

const server = express();

server.get('/log', (request, response) => response.sendFile(process.cwd() + '/bloggo.log'));

server.get('/', async (request, response) => {
  await log('Rendering blog page…');
  const title = configuration.title || 'Blog';
  const posts = [];
  for (const repository of repositories) {
    const slugUrl = repository.name;
    const repoUrl = repository.webUrl;
    const title = repository.title || repository.name;
    const date = repository.date;
    const description = repository.description;
    const service = repository.service;
    const change = repository.changeset[0];
    posts.push({ slugUrl, repoUrl, title, date, description, service, change });
  }

  response.send(executeJavaScriptFunction(configuration.template || await getDefaultBlogTemplateString(), { title, posts }) + versionHtml);
});

server.get('/post/:name', async (request, response) => {
  const name = request.params['name'];
  await log(`Rendering post page for ${name}…`);
  if (!isValidName(name)) {
    response.status(404).redirect('/');
    return;
  }

  const repository = repositories.find(r => r.name === name);
  if (!repository) {
    response.status(404).redirect('/');
    return;
  }

  const blogTitle = configuration.title || 'Blog';
  const title = repository.title || repository.name;
  const slugUrl = repository.name;
  const repoUrl = repository.webUrl;
  const date = repository.date;
  const description = repository.description;
  const service = repository.service;
  const html = repository.html;
  const changeset = repository.changeset;

  response.send(executeJavaScriptFunction(repository.configuration.template || await getDefaultPostTemplateString(), { blogTitle, title, slugUrl, repoUrl, date, description, service, html, changeset }) + versionHtml);
});

const port = process.env.PORT ? Number(process.env.PORT) : 3000;
server.listen(port, async () => {
  await clear();
  await log(`Listening on ${port}…`);
  for await (const repository of watch(services, 60)) {
    await log(`Processsing ${repository.service.toString()} > ${repository.name}`);
    try {
      await repository.ensureUpToDateDirectory();
    } catch (error) {
      await log(`Failing ${repository.toString()}: ${error}`);
      continue;
    }

    await repository.refreshConfiguration(); // 1st so that HTML can be taken from configured index file
    await repository.refreshTemplate(); // 2nd so that the template file path can be decided based on the configuration
    await repository.refreshHtml(); // 3rd so that title can be read from the HTML element
    repository.refreshTitle();

    if (repository.name === 'bloggo') {
      await log('Found Bloggo!');

      if (repository.title !== null) {
        configuration.title = repository.title;
      }

      if (repository.template !== null) {
        configuration.template = repository.template;
      }

      configuration = repository.configuration;
      // TODO: Serve Bloggo static assets if not served already or if bloggo service changed from the last time
    } else {
      await repository.refreshChangeset();

      const index = repositories.findIndex(r => r.id === repository.id && r.service === repository.service);
      if (index === -1) {
        repositories.push(repository);
        await log(`Discovering ${repository.toString()}`);
        // TODO: Start serving static assets by replacing router https://github.com/expressjs/express/issues/2596
        server.use(`/post/${repository.name}`, express.static(repository.path));
        await log(`Serving ${repository.toString()}…`);
      } else {
        repositories[index] = repository;
        await log(`Refreshing ${repository.toString()}`);
      }

      repositories.sort((a, b) => b.changeset[0].author.timestamp - a.changeset[0].author.timestamp);
    }
  }
});
