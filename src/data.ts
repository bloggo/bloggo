import * as vm from 'vm';
import * as fs from 'fs-extra';

export function isValidName(name: string) {
	return name.match(/^[a-z0-9-]+$/);
}

export function executeJavaScriptFunction(javaScript: string, args: object) {
  const context = { args, result: undefined };
  vm.createContext(context);
  try {
    vm.runInContext(`result = (${javaScript})(args)`, context);
    return context.result;
  } catch (error) {
    console.log(`Failed to execute JavaScript function. ${javaScript} ${args} ${error}`);
  }
}

let defaultBlogTemplate: string;
let defaultPostTemplate: string;

export async function getDefaultBlogTemplateString() {
	if (!defaultBlogTemplate) {
    defaultBlogTemplate = `({ title, posts }) => \`${await fs.readFile('src/template/blog.html')}\``;
	}

	return defaultBlogTemplate;
}

export async function getDefaultPostTemplateString() {
	if (!defaultPostTemplate) {
    defaultPostTemplate = `({ blogTitle, title, slugUrl, repoUrl, date, description, service, html, changeset }) => \`${await fs.readFile('src/template/post.html')}\``;
	}

	return defaultPostTemplate;
}
