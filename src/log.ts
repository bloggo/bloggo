import * as fs from 'fs-extra';

export async function log(message: string) {
	console.log(message);
	const stack = (new Error().stack || '').split('\n').filter(line => {
		return line !== 'Error' && !line.includes('log.ts') && !line.includes('log.js') && (line.includes('.ts:') || line.includes('.js:'));
	}).join('\n');
	await fs.appendFile('bloggo.log', new Date().toLocaleString() + ' ' + message + '\n' + stack + '\n');
}

export async function clear() {
	await fs.remove('bloggo.log');
}
