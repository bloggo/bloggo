# Convert bloggo users on GitHub and GitLab to org and group

It is possible to convert a GitHub user to a GitHub org.

It is [currently not possible](https://gitlab.com/gitlab-org/gitlab-ce/issues/25379)
to convert a GitLab user to a GitLab group.

Do not convert either user until both can be at the same time.
