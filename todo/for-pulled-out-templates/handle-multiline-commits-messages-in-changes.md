# Handle multiline commit messages in *Changes*

Like *Remove obsolete task* [here](http://hubelbauer.net/post/net-tree).

It should be a line a paragraph like in other places that show commit messages.
