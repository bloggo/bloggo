# Update the base tag with respect to the entry nesting

`entry` may be a file nested within the repository and the `base` tag
needs to be adjusted accordingly, so that all the files stay relative
to the `entry` file the same way they are in the repository.

For a file in the root directory of the repository `test`, base should
be `test/`.

For a file nested as `doc/post` within the repository, base should be
`test/doc/post/`.
