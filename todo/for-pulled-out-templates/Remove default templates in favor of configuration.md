# Remove default templates in favor of configuration

The `bloggo` repository should not contain any templates.
Instead, the `bloggo` configuration repository should contain them and when they are found, that's when the app starts displaying content.
In case of my instance, these two repositories collide into one.
Probably it will be the best to move Bloggo under its own organization/group (or Rhaeo) and do development there and keep Bloggo configuration repository here.
