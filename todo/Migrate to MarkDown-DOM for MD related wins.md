# Migrate to MarkDown-DOM for MD related wins

- [ ] Fix the titles that are also links and currently end up being an `a` wrapped in another `a` in which case clicking the post link leads to the MarkDown title link and not the post page
- [ ] Add permalinking to headers and have nice `#` URLs (the current solution with Showdown generates ugly dashless links)
- [ ] Ability to check links for broken rooted thought relative paths like `/doc/tasks`
- [ ] Ability to override `.md` links with `.md.html` auto-generated files
- [ ] Get plain text value of the title heading sans the formatting for the document title
- [ ] Introduce ability to check links for link rot (either just links within the site or maybe external links as well)
- [ ] Make images clickable to open in a lightbox or a new tab
- [ ] Allow for code blocks, tables and images to be expansible to the full width of the document
  - For images it would be done by attaching `onload`, checking their automatic width and if > than content column width, making the sole image within its own block full-width
    - This should probably ignore images that are not alone in the block
  - For tables and code blocks there should be a button to flip between content-width and full-width states
- [ ] When we have MarkDown-DOM in and rewrite links to other MarkDown files to instead render them as HTML, too, link to the MarkDown file for the specific page, not just README.
