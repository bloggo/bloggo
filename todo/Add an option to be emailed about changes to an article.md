# Add an option to be emailed about changes to an article

- [ ] Distinguish legitimate update to a repository (currently its all clones)
- [ ] Keep a list of subscribers (S3 or a Dokku storage plugin)
- [ ] Send out the emails using MailGun using some sort of a bulk API if available
