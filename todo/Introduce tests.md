# Introduce tests

- `bloggo/template.js` broken and missing
- `bloggo/configuration.json`
  - `title` empty, missing (default `Blog`)
  - `syncIntervalMinutes` invalid, missing (default `10`)
- `.bloggo/template.js` broken and missing
- `.bloggo/configuration.json`
  - `entry` invalid, valid `.md`, valid `.html`, missing (default `README.md`)
- Using GitHub org
- Using GitLab group
- Use Git Node server for mock repositories
