# Redo routes to be able to start and stop serving files as will

This should be done by [replacing the router](https://github.com/expressjs/express/issues/2596).

- [ ] Stop serving statics of deleted posts
