# Bloggo

> Bloggo scans GitHub and GitLab repositories for given handles and serves repositories with a `.bloggo` directory.

**Features:**

- Scans GitHub user/org and GitLab user/group accounts for repositories containing Bloggo configuration directory `.bloggo`
- Sorts posts by the most recent contribution to the `master` branch
- Allows for configuring blog and post templates using `.bloggo/configuration.json` file

**Limitations:**

- Supports public repositories only (design choice)
- Only considers the `master` branch (design choice)

## Installing

| Prerequisite | Minimal Version | Optimal Version |
|--------------|-----------------|-----------------|
| NodeJS       | ?               | 9.4.0           |

[Support for self-hosting is under development.](doc/tasks.md)

## Running

[Bloggo demo](http://hubelbauer.net/)

| Prerequisite | Minimal Version | Optimal Version | Justification                                | Comment                                            |
|--------------|-----------------|-----------------|----------------------------------------------|----------------------------------------------------|
| NodeJS       | ?               | 9.8.0           | Application runtime (see [`.nvmrc`](.nvmrc)) | Keep in sync with `engines.node` in `package.json` |
| Yarn         | ?               | 1.5.1           | Usable package manager                       | Keep in sync with `engines.yarn` in `package.json` |
| `nodemon`    | ?               | ?               | Change watcher                               |                                                    |
| TypeScript   | ?               | 2.7.2           | TypeScript to JavaScript compiler            | Needs to be in `dependencies` for Dokku it appears |

```sh
yarn run dev
open http://localhost:3000/ # `start` on Windows
```

See [Configuring](#configuring) for the command line interface switch reference.

## Testing

```sh
yarn test
open http://localhost:3000/ # `start` on Windows
```

[Introduction of automated tests in under development.](doc/tasks.md)

## Configuring

```sh
yarn start -h
yarn start --help
yarn start -v
yarn start --version
```

Service accoung handles whose repositories to scan:

- `--github-user`: a GitHub user
- `--github-org`: a GitHub organization
- `--gitlab-user`: a GitLab user
- `--gitlab-group`: a GitLab group

The first of account to have `bloggo` repository will have it used.

**`configuration.json`** in `bloggo` repository:

- `title`: blog title (default: `Blog`)
- `delay`: delay between synchronization cycles (in seconds, default: `60`)
- `template`: JavaScript blog page template function (default: use `template.js`)

**`configuration.json`** in `.bloggo` repository directory:

- `entry`: post page repository relative file path (`.md` or `.html`, default: `README.md`)
- `template`: JavaScript blog page template function (default: use `template.js`)

For `template` or `template.js` function argument regerence see [Templating](#templating).

## Templating

Blog page (`template.js` or `template` in `configuration.json` of `bloggo` repository from the configured source service):

- `title`: configured blog title
- `posts[]`: posts
  - `url`: slug URL
  - `name`: post name (either pulled from the main heading or the same as slug URL if no heading)
  - `date`: creation date
  - `description`: service repository description string
  - `change`: top `git log` entry, verbatim from [`isomorphic-git`](https://isomorphic-git.github.io/docs/log.html)

Post page (`template.js` or `template` in `configuration.json` of `.bloggo` repositories from cofigured service handles):

- `title`: configured blog title
- `name`: post name (either pulled from the main heading or the same as slug URL if no heading)
- `slugUrl`: slug URL
- `repoUrl`: service repository URL (GitHub, GitLab)
- `date`: creation date
- `description`: service repository description string
- `html`: verbatim content of `entry` if `entry` is HTML, otherwise converted HTML if `entry` is MarkDown (like the default `README.md`)
- `changeset[]`: `git log`, verbatim from [`isomorphic-git`](https://isomorphic-git.github.io/docs/log.html)

## Routing

The posts reside under `/post`. This is so that post names do not clash with other routes.

Cross-posting using relative URLs is not possible, because in GitHub and GitLab, posts reside under the handle.
On top of that, the `bloggo-` prefix is cut in Bloggo URLs but obviously is present in the service URLs.
There is no good solution to this yet, I opt to linking both the Bloggo post and the service repository currently.
In principle, linking just the Bloggo post is fine as well, you can always click through to the service URL from it.

## Licensing

This repository is licensed under the terms of the [MIT license](LICENSE.md).

## Contributing

Conduct: *don't do what I wouldn't condone*.

Consult [todo](todo) and [wiki](wiki).

The application is hosted on GitHub _and_ GitLab. See `git remote -v`:

- `git push` [`github`](https://github.com/TomasHubelbauer/bloggo) `master`
- `git push` [`gitlab`](https://gitlab.com/TomasHubelbauer/bloggo) `master`

Contribute to either, we keep both origins in sync manually.
Do not add a `origin` remote and in VS Code integrated Git UI, use *Push to…*.

The repositories are owned by user accounts named `bloggo`.
We will convert these to GitHub org and GitLab group respectively at some point.
Currently it is not possible to convert a GitLab user to a GitLab group.

## Deploying

See [Installing](#installing) for self-hosting.

[Tomas Hubelbauer](http://hubelbauer.net/) (single tenant instance, multitenant instance work in progress)

```sh
yarn run check
wsl
git push dokku master
```

Bloggo will eventually be hosted on [Bloggo.net](https://bloggo.net).
