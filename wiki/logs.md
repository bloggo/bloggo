# Logs

## 2018-03-14

Decided not to support connecting to local installations of services from the client app,
instead the suggested solution is to run Bloggo locally and point to local as well as remote services as preferred.

Set [`DOKKU_DEFAULT_CHECKS_WAIT`](https://github.com/dokku/dokku/blob/master/docs/deployment/zero-downtime-deploys.md#configuring-check-settings-using-the-config-plugin)
to 60 seconds to give TSC time to compile TypeScript to JavaScript and then NodeJS to start and Express to bind the port.

Ignored the idea of repository contents pagination for now as it's super unlikely the `.bloggo` directory wouldn't be in the first page of the results.

## 2018-03-13

Decided to ditch the post `exec` idea, posts will remain static and won't be applications.
We can leave generated artifacts in tree where sufficient or deploy to Dokku where a full application makes sense and link to it from the post.

## 2018-01-28

Moved Bloggo to a Dokku instance on Hubelbauer.net.

## 2018-02-13

Decided to serve posts at `/post/:name` to not have to worry about post names conflicting with routes.

Revisited the idea of using web workers to fetch repos in parallel with the main logic, but deemed it
too inflexible and ultimately unnecessary. The API loop can use a lot of work, but I'll keep it async
but sequential as it is now.

## 2018-01-25

Drafted a new version of repository fetching logic which iterates over numerous variants and
attempts to utilize the maximum of the bandwidth.

## 2018-01-20

Upgraded the Heroku dyno to the Hobby plan for 7 USB a month.
This is a result of implementing the repository persistence logic only to realize the cache file
will also get scraped when the dyno goes to sleep and Heroku doesn't seem to have any volumes to
use, so instead I will benefit from the persistence when testing locally and pay for a Hobby dyno.

Disregarded the idea of using hooks, because:

- It would be harder to set up compared to just entering user name and going
- On GitLab system hooks for new repository notifications are not available on GitLab.com

## 2018-01-09

Started the project and drafted cloning and pulling repositories, serving index and post pages.
